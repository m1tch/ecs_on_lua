-- Idea: move components to "namespace" Components, and then ECS Manager class 
-- can automatically create arrays for every type of components.

--- Just a template for component
function Component_Empty() 
	local self = {
		name = "put_unique_name"
		-- properties ...
	}
	return self
end

function Component_Position(_x, _y)
	local self = {	
		name = "position",
		x = _x or 0, 
		y = _y or 0 }
	return self
end

function Component_Velocity(_vx, _vy)
	local self = {
		name = "velocity",
		vx = _vx or 0,
		vy = _vy or 0 }
	return self
end

function Component_Keyboard_Controls(_up, _right, _down, _left) 
	local self = {
		name = "keyboard_controls",
		up = "up" or _up,
		right = "right" or _right,
		down = "down" or _down,
		left = "left" or _left
	}
	return self
end

function Component_Timer_Event(_remaining_time, _system, _component, _args)
	local self = {
		name = "timer_event",
		remaining_time = _remaining_time, -- in seconds
		system = _system, 
		component = _component, 
		args = _args
	}
	return self
end

function Component_New_Direction(args)

	local self = {
		name = "new_direction",
		vx = args.vx or 0,
		vy = args.vy or 0 
	}
	return self
end


