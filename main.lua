require "ECS_Manager"
local isubs = require "iterator_subscribers"
--- Assembling custom entity
function create_player(x, y, vx, vy)
	local entity = ecs.newEntity()
	ecs.addComponent(entity, Component_Position(x, y))
	ecs.addComponent(entity, Component_Velocity(vx, vy))
	ecs.addComponent(entity, Component_Keyboard_Controls())
	return entity
end

function create_moving_dot(x, y, vx, vy)
	local entity = ecs.newEntity()
	ecs.addComponent(entity, Component_Position(x, y))
	ecs.addComponent(entity, Component_Velocity(vx, vy))
	return entity
end

function create_static_dot(x, y)
	local entity = ecs.newEntity()
	ecs.addComponent(entity, Component_Position(x, y))
	return entity
end

function create_physical_box(ECS, x, y, w, h, vx, vy)
	local entity = ECS.newEntity()
	ECS.addComponent(entity, Component_Physical_Box(x, y, w, h, 0, 100))
	ECS.addComponent(entity, Component_Physical_Movement(vx, vy))
	return entity
end

function add_all_systems(ECS)
	for key, system in pairs(Systems) do
		if key ~= "Base" then
			ECS.addSystem(system())
		end
	end
end

function add_systems_prior(ECS)
	ECS.addSystem(Systems.Handling())
	ECS.addSystem(Systems.Moving())
	ECS.addSystem(Systems.EventTimerHandling())
	ECS.addSystem(Systems.ChangeDirection())
	ECS.addSystem(Systems.Drawing())
end

-- Main --
-- testing ECS framework

function love.load()
	ecs = ECSManager()
	add_systems_prior(ecs)
	e1 = create_player(400, 300)
	create_player(100, 100)
	e2 = create_moving_dot(300, 300, 50, 0)
	ecs.addEventComponent(e2, Component_Timer_Event(1.0, "change_direction", 
		Component_New_Direction, {vx=love.math.random(-1, 1)*30, vy=love.math.random(-1,1)*30}))
	create_static_dot(500, 400)

end

function love.update(dt)
	ecs.update(dt)
end

function love.draw()
	ecs.draw()
end
