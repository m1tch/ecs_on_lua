ECS Machine 
(work in progress)

ECS Machine is minimal Entity-Component-System framework written on Lua and
designed to use with Love2D game engine.

Basic functionality is already implemented, but the framework requires more 
testing, bug fixing and maybe some functionality adding.