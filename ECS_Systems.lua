require "ECS_Components"
local iter_select = require "iterator_select"
local isubs = require "iterator_subscribers"

local Systems = {}

-- Subscribers list optimization idea: update list only when new component is added to entity
-- So, there is no need to request subscribers list every update

function Systems.Base()
	-- This is not a template! Custom systems must be _inherited_ from this class 
	-- Don't forget about returning self table
	local self = {}
	self.name = "base"
	local manager = {}
	local subscribers = {}
	local entities = {}
	
	-- Mb there is a better solution to add/remove components and (un)subscribe to another system from self_system?
	function self.addComponent(entity, component)
		manager.addComponent(entity, component)
	end
	
	function self.removeComponent(entity, component)
		manager.removeComponent(entity, component)
	end
	
	function self.addEventComponent(entity, component)
		manager.addEventComponent(entity, component)
	end
	
	function self.removeEventComponent(entity, component, index)
		manager.removeEventComponent(entity, component, index)
	end
	
	function self.subscribe(entity)
		table.insert(subscribers, entity)
	end
	
	function self.unsubscribe(entity)
		for i, e in ipairs(subscribers) do
			if e == entity then
				table.remove(subscribers, i)
				--entities[i] = nil
			end
		end
	end
	
	function self.subscribeToAnother(entity, system)
		manager.subscribe(entity, system)
	end

	function self.set_entities(_entities)
		entities = _entities
	end
	
	function self.get_entities()
		--return {entities, subscribers}
		return entities
	end
	
	function self.set_manager(_manager)
		manager = _manager
	end
	
	function self.get_manager()
		return manager
	end
	
	return self
end

function Systems.Moving()
	local self = Systems.Base()
	self.name = "moving"
	
	local requirements = {
		"position",
		"velocity"
	}
	
	function self.update(dt)
		local entities = self.get_entities()
		for i, e in isubs(entities, requirements) do
			local p = e.position
			local v = e.velocity
			p.x = p.x + v.vx * dt
			p.y = p.y + v.vy * dt
		end
	end
	
	return self
end

function Systems.Handling()
	local self = Systems.Base()
	self.name = "handling"
	
	local requirements = {
		"velocity",
		"keyboard_controls"
	}
	
	function self.update(dt)
		local entities = self.get_entities()
		for i, e in isubs(entities, requirements) do
			local v = e.velocity
			local b = e.keyboard_controls
			v.vx, v.vy = 0, 0
			if 		love.keyboard.isDown(b.up) 		then v.vy = -100
			elseif 	love.keyboard.isDown(b.down) 	then v.vy = 100
			end
			if 		love.keyboard.isDown(b.right) 	then v.vx = 100
			elseif 	love.keyboard.isDown(b.left) 	then v.vx = -100
			end
		end
	end
	
	return self
end

function Systems.EventTimerHandling()
	local self = Systems.Base()
	self.name = "event_timer_handling"
	
	local requirements = {
		"timer_event"
	}
	
	function self.update(dt)
		local entities = self.get_entities()
		for i, e in isubs(entities, requirements) do
			local events = e.timer_event
			for j, te in ipairs(events) do
				te.remaining_time = te.remaining_time - dt
				if te.remaining_time <= 0 then
					self.addComponent(i, te.component(te.args))
					self.removeEventComponent(i, te.name, j)
				end
			end
		end
	end
	
	return self
end

function Systems.ChangeDirection()
	local self = Systems.Base()
	self.name = "change_direction"
	
	local requirements = {
		"new_direction",
		"velocity"
	}
	
	function self.update(dt)
		local entities = self.get_entities()
		for i, e in isubs(entities, requirements) do
			local v = e.velocity
			local nd = e.new_direction
			v.vx, v.vy = nd.vx, nd.vy

			self.addEventComponent(i, 
				Component_Timer_Event(1.0, "change_direction", 
					Component_New_Direction, {vx=love.math.random(-1, 1)*30, vy=love.math.random(-1,1)*30}))
					
			self.removeComponent(i, nd.name)
		end
	end
	
	return self
end

function Systems.Drawing()
	local self = Systems.Base()
	self.name = "drawing"
	
	local requirements = {
		"position"
	}
	
	function self.draw()
		local entities = self.get_entities()
		--print("there", #entities)
		for i, e in isubs(entities, requirements) do
			local p = e.position
			love.graphics.circle("fill", p.x, p.y, 10)
		end
	end
	
	return self
end


return Systems