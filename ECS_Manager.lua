Systems = require "ECS_Systems"
require "ECS_Components"

local function table_clear(t)
	for k, v in pairs(t) do t[k] = nil end
end

--to do: for all remove methods do a pull of objects, that must deleted and delete them after all updatings

function ECSManager()
	local self = {}
	local systems = {}
	local entities = {}
	
	local entitiesToDelete = {}
	local componentsToDelete = {}
	local eventsToDelete = {}
	local systemsToDelete = {}
	
	function self.addAllSystems()
		for i, s in pairs(Systems) do
			local sys = s()
			sys.set_entities(entities)
			sys.set_manager(self)
			systems[sys.name] = sys
		end
	end
	
	function self.addSystem(system)
		system.set_entities(entities)
		system.set_manager(self)
		table.insert(systems, system)
	end
	
	function self.removeSystem(system)
		-- perhaps, code is not so safe (what if delete system while it running)
		-- (same problem with components and entities ...)
		--[[for index, sys in pairs(systems) do
			if sys.name == system then
				systems[index] = nil
				return
			end
		end]]
		table.insert(systemsToDelete, system)
	end
	
	function self.newEntity()
		table.insert(entities, {})
		return #entities
	end
	
	function self.deleteEntity(entity)
		table.insert(entitiesToDelete, entity)
	end
	
	--- Add instance of component to entity
	function self.addComponent(entity, component)
		entities[entity][component.name] = component
	end
	
	--- Remove component from entity by name
	function self.removeComponent(entity, component)
		--entities[entity][component] = nil
		table.insert(componentsToDelete, {entity=entity, component=component})
	end
	
	function self.addEventComponent(entity, component)
		if entities[entity][component.name] == nil then
			entities[entity][component.name] = {}
		end
		table.insert(entities[entity][component.name], component)
	end
	
	function self.removeEventComponent(entity, component, index)
		--[[entities[entity][component][index] = nil
		if #entities[entity][component] == 0 then
			entities[entity][component] = nil
		end]]
		table.insert(eventsToDelete, {entity=entity, component=component, index=index})
	end
	
	local function deleteMarkedObjects()
		for k, v in ipairs(entitiesToDelete) do
			table.remove(entities, v)
		end
		table_clear(entitiesToDelete)
		for k, v in ipairs(componentsToDelete) do
			if entities[v.entity] then entities[v.entity][v.component] = nil end
		end
		table_clear(componentsToDelete)
		for k, v in ipairs(eventsToDelete) do
			if entities[v.entity] then table.remove(entities[v.entity][v.component], v.index) end
		end
		table_clear(eventsToDelete)
		for k, system in ipairs(systemsToDelete) do
			for index, sys in pairs(systems) do
				if sys.name == system then
					systems[index] = nil
					break
				end
			end
		end
		table_clear(systemsToDelete)
	end
	
	function self.update(dt)
		for k, system in pairs(systems) do
			local upd = system.update
			if upd then	upd(dt)	end
		end
		deleteMarkedObjects()
	end
	
	function self.draw()
		for k, system in pairs(systems) do
			local drw = system.draw
			if drw then drw(dt) end
		end
	end
	
	return self
end
