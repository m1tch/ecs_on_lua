--- Iterates over entities that meets the requirements
function isubscribers(entities, requirements, start)
	local i = start or 1
	
	i = i-1
	
	return function ()
		local entity = {}
		if i > #entities then
			return nil, nil, nil
		end
		
		i, entity = next(entities, i)

		while entity do
			local have_all = true
			
			for _, comp in pairs(requirements) do
				if entity[comp] == nil then
					have_all = false
					break
				end
			end
			
			if have_all then 
				return i, entity, nil
			end
			
			i, entity = next(entities, i)
		end
		
		return i, entity, nil
	end
end

return isubscribers