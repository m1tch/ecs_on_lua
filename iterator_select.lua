--- Iterator for iterating _objects_ only with selected _indexes_
-- Function takes two tables: objects and indexes
-- Tables can be passed in one table
function iter_select(...)
	local i = 0
	local arg = {...}
	local objects, indexes
	
	if #arg == 2 then
		objects, indexes = arg[1], arg[2]
	elseif #arg[1] == 2 then
		objects, indexes = arg[1][1], arg[1][2]
	end

	
	-- "next" element function
	return function ()
		local v, index = {}, nil
		if #indexes == 0 then
			return
		end
		i, index = next(indexes, i)
		while index do
			v = objects[index]			
			if v then 
				return index, v, nil
			end
			i, index = next(indexes, i)
		end
		return index, v, nil
	end
end

return iter_select